import { HydratedGetCatsProvider } from "./components";
import { CatsList } from "./components";

export default function Home() {
  return (
    <HydratedGetCatsProvider>
      <CatsList />
    </HydratedGetCatsProvider>
  );
}
