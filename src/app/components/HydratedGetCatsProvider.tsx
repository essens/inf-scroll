import {
  getQueryClient,
  dehydrate,
  HydrationBoundary,
} from "@/api/query-client";
import { getCats, keys as catsApiKeys } from "@/api/cats";
import { CATS_LOAD_PER_PAGE } from "../constants";

export default async function HydratedGetCatsProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const queryClient = getQueryClient();

  const queryArgs = { limit: CATS_LOAD_PER_PAGE, page: 1 };

  await queryClient.prefetchQuery({
    queryKey: catsApiKeys.get(queryArgs),
    queryFn: () => getCats(queryArgs),
  });

  const dehydratedState = dehydrate(queryClient);

  return (
    <HydrationBoundary state={dehydratedState}>{children}</HydrationBoundary>
  );
}
