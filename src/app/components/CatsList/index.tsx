"use client";

import { useIntersectionObserver } from "@/app/hooks";
import { useGetCatsInfiniteQuery } from "@/api/cats";
import { CATS_LOAD_PER_PAGE } from "@/app/constants";
import { useCallback, useEffect, useMemo, useState } from "react";
import { CatItemType } from "@/api/cats/types";
import Button from "../Button";
import Item from "./Item";

const DISABLE_AUTO_FETCHING_AT_PAGE = 5;

export default function CatsList() {
  const [isAddingNewItems, setAddingNewItems] = useState(false);

  const {
    pages,
    isFetching,
    currentPageParam,
    hasNextPage,
    fetchNextPage: basefetchNextPage,
  } = useGetCatsInfiniteQuery({
    limit: CATS_LOAD_PER_PAGE,
  });

  const fetchNextPage = useCallback(() => {
    setAddingNewItems(true);
    basefetchNextPage();
  }, [basefetchNextPage]);

  const ref = useIntersectionObserver(
    (isIntersecting) => {
      if (
        isIntersecting &&
        hasNextPage &&
        !isFetching &&
        !isAddingNewItems &&
        currentPageParam !== DISABLE_AUTO_FETCHING_AT_PAGE
      ) {
        fetchNextPage();
      }
    },
    { threshold: 0.5 }
  );

  useEffect(() => {
    if (isAddingNewItems && !isFetching) {
      setAddingNewItems(false);
    }
  }, [isAddingNewItems, isFetching]);

  const itemsWithPlaceholders = useMemo(() => {
    const catItems = pages.reduce<(CatItemType | undefined)[]>(
      (acc, page) => [...acc, ...page.data],
      []
    );

    if (isAddingNewItems) {
      const placeholderItems: undefined[] = Array.from({
        length: CATS_LOAD_PER_PAGE,
      });

      catItems.push(...placeholderItems);
    }

    return catItems;
  }, [pages, isAddingNewItems]);

  return (
    <section>
      {itemsWithPlaceholders.map((item, index) => (
        <Item
          key={`${item?.id ?? "placeholder"}${index}`} // API can return duplicates. Use index to make it unique.
          item={item}
          isInitialLoad={currentPageParam === 1} // For SEO purposes, load all images on the first page without placeholders
        />
      ))}
      {!isAddingNewItems && hasNextPage && (
        <div className="my-2">
          {currentPageParam === DISABLE_AUTO_FETCHING_AT_PAGE ? (
            <Button onClick={fetchNextPage}>Keep em coming</Button>
          ) : (
            <div ref={ref}>Loading more..</div>
          )}
        </div>
      )}
    </section>
  );
}
