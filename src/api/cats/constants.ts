import { GetArgsType } from "./types";

export const endpoints = {
  get: (args: GetArgsType) =>
    `/images/search?limit=${args.limit}&page=${args.page}`,
};

export const keys = {
  get: (args: GetArgsType) => ["cats", args],
};
