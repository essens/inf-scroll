"use client";

import { useInfiniteQuery } from "@/api/query-client";
import { GetArgsType, GetResponseType } from "../types";
import { keys } from "../constants";
import { getCats } from "../fetchers";
import { useCallback } from "react";

export default function useGetCatsInfiniteQuery(
  args: Omit<GetArgsType, "page">
) {
  return useInfiniteQuery<GetResponseType>({
    queryKeyFn: (page) => keys.get({ ...args, page }),
    queryFn: ({ pageParam }: { pageParam: number }) =>
      getCats({ ...args, page: pageParam }),
    getNextPageParam: useCallback(
      (currentPageParam, lastPage) => {
        if (!lastPage) {
          return null;
        }

        return lastPage.length >= args.limit ? currentPageParam + 1 : null;
      },
      [args.limit]
    ),
    initialPageParam: 1,
  });
}
