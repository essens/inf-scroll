import { endpoints } from "../constants";
import { GetArgsType, GetResponseType } from "../types";
import baseFetch from "./baseFetch";

export default async function getCats(
  args: GetArgsType
): Promise<GetResponseType> {
  return baseFetch(endpoints.get(args));
}
