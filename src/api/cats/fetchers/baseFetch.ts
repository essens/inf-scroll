export default async function baseFetch(
  url: URL | RequestInfo,
  options?: RequestInit
) {
  const response = await fetch(
    `${process.env.NEXT_PUBLIC_CATS_API_BASE_URI}${url}`,
    {
      ...options,
      headers: {
        "x-api-key": process.env.NEXT_PUBLIC_CATS_API_KEY as string,
        ...options?.headers,
      },
    }
  );

  if (!response.ok) {
    throw new Error("API Error");
  }

  return response.json();
}
