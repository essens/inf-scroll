export type GetArgsType = {
  limit: number;
  page: number;
};

export type CatItemType = {
  id: string;
  url: string;
  width: number;
  height: number;
};

export type GetResponseType = CatItemType[];
