"use client";

import { useCallback, useEffect, useMemo, useState } from "react";
import { useQuery } from "..";
import type { QueryKey, UseQueryResult } from "@tanstack/react-query";

type InfiniteQueryInfo<T> = {
  fetchNextPage: () => Promise<void>;
  hasNextPage: boolean;
  currentPageParam: number;
  pages: { pageParam: number; data: T }[];
};

export default function useInfiniteQuery<T>(queryArgs: {
  queryKeyFn: (pageParam: number) => QueryKey;
  queryFn: any;
  getNextPageParam: (currentPageParam: number, lastData?: T) => number | null;
  initialPageParam: number;
}): UseQueryResult<T> & InfiniteQueryInfo<T> {
  const { getNextPageParam, queryKeyFn, queryFn } = queryArgs;

  const [currentPageParam, setCurrentPageParam] = useState<number>(
    queryArgs.initialPageParam
  );

  const queryData = useQuery<T>({
    queryKey: queryKeyFn(currentPageParam),
    queryFn: () => queryFn({ pageParam: currentPageParam }),
  });

  const [pages, setPages] = useState<{ pageParam: number; data: T }[]>(
    queryData.data
      ? [{ pageParam: currentPageParam, data: queryData.data }]
      : []
  );

  useEffect(() => {
    if (
      queryData.isSuccess &&
      !pages.find(({ pageParam }) => pageParam === currentPageParam)
    ) {
      setPages((prevPages) => [
        ...prevPages,
        { pageParam: currentPageParam, data: queryData.data },
      ]);
    }
  }, [currentPageParam, pages, queryData.data, queryData.isSuccess]);

  const isFetching =
    (queryData.isFetching ||
      !pages.find(({ pageParam }) => pageParam === currentPageParam)) &&
    !queryData.isSuccess;

  const nextPageParam = useMemo(
    () => getNextPageParam(currentPageParam, queryData.data),
    [getNextPageParam, queryData.data, currentPageParam]
  );

  const fetchNextPage = useCallback(async () => {
    if (nextPageParam == null) return;

    setCurrentPageParam(nextPageParam);
  }, [nextPageParam]);

  return useMemo(
    () => ({
      ...queryData,
      isFetching,
      currentPageParam,
      fetchNextPage,
      pages,
      hasNextPage: nextPageParam != null,
    }),
    [
      queryData,
      isFetching,
      currentPageParam,
      fetchNextPage,
      pages,
      nextPageParam,
    ]
  );
}
