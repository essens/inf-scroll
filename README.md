## Requirements

- Node v20.11 and up.

## Installation

- Copy `.env.example` to `.env` and fill in the required values
- Run `npm install`
- Done!

## Development

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Deployment

Deployment process is automated using Vercel. Once changes are pushed to version control, Vercel CI/CD handles the rest.
