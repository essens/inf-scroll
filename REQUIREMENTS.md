Create a one page app with an infinite scrolling list of image cards.
Page should be SEO friendly and responsive.
API url:
https://api.thecatapi.com/v1/images/search?limit=24
API key: live_ZUDIUh8Jz0OZOwidsJYMwBHc6nOEAXXmRgvCQKuic68I3McM46wLmQjFIv9E20lH
Use it as the 'x-api-key' header when making any request to the API
Requirements:

- The app should be bootstrapped with Next.js.
- The app should use Tailwind for CSS.
- The app should use the React Query library for data fetching.
- The app should use the provided API to fetch items.
- The app should implement infinite scroll with IntersectionObserver without external npm packages.
- Each card should display an image of the cat.
- Each time the user scrolls to the bottom of the page, new cards should be fetched, and placeholders should be displayed while data is loading.
- After the first 5 pages are fetched, auto-fetch functionality should be stopped, and a button should appear at the end of the page to see more items. This is similar to the implementation on the
  Google.com
  search page.
- After clicking on the button, auto-fetch functionality should resume and continue until the user reaches the end of the items list.
  Bonus feature:
- Inject a Google Ad after 3 image cards on each page using React code
  Example of ad implementation:
  https://developers.google.com/publisher-tag/samples/ad-sizes?hl=en#javascript
  Code should be publicly accessible in a git repository.
  Do not reference Zedge in the code.
